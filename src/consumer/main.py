from kombu import Connection, Queue
import os


def process_msg(foo):

    with Connection(os.environ['BROKER_URL']) as _conn:
        sub_queue = _conn.SimpleQueue(str(foo))
        while True:
            _msg = sub_queue.get(block=False)
            print(_msg.payload)
            _msg.ack()

        sub_queue.close()
        chan = _conn.channel()
        dq = Queue(name=str(foo), exchange="")
        bdq = dq(chan)
        bdq.delete()


with Connection(os.environ['BROKER_URL']) as conn:
    rec = conn.SimpleQueue('control_queue')
    while True:
        msg = rec.get(block=True)
        entry = msg.payload
        msg.ack()
        process_msg(entry)
