from kombu import Connection
import uuid
from time import sleep
import os


def publish_data(conn):
    random_name = "q" + str(uuid.uuid4()).replace("-", "")
    random_queue = conn.SimpleQueue(random_name)
    for i in range(0, 42):
        random_queue.put(i)
    random_queue.close()
    return random_name


with Connection(os.environ['BROKER_URL']) as conn:
    control_queue = conn.SimpleQueue('control_queue')
    _a = 0
    while True:
        y_name = publish_data(conn)
        message = y_name
        control_queue.put(message)
        print('Sent: {0}'.format(message))
        _a += 1
        sleep(0.3)
        if _a > 20:
            break

    control_queue.close()
