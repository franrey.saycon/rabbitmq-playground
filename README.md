# rabbitmq-playground

Two celery apps communicating through RabbitMQ. 

## Setup
Note: This was setup in a Windows Environment

Install
1. Docker Desktop
2. Run `docker compose up --build`

## Usage
1. Go to `localhost:5673` to with User: admin, Pass: mypass to check the rabbitmq status.
2. Works should continually do a heartbeat tick every 5 seconds.
